<?php

namespace Twista\codeKata;


/**
 * Interface IObserver
 * @package Twista\codeKata
 */
interface IObserver {
    public function update(IObservable $observable);
}