<?php
namespace Twista\codeKata;

/**
 * Class MinutesLight
 * @package Twista\codeKata
 */
class MinutesLight implements IObserver {

    const INDEX = 1;

    /**
     * @param IObservable $observable
     */
    public function update(IObservable $observable) {
        if(!$observable instanceof IDiodeClock)
            return;

        $time = $this->resolveTime($observable->getTimeFragment(self::INDEX));
        $observable->setLights(self::INDEX, $time);
    }

    /**
     * @param $time
     * @return string
     */
    protected function resolveTime($time){
        // compose first row of 11 diodes
        $fiveMultiples = floor($time / 5);
        $firstRow = str_repeat('Y',$fiveMultiples);
        $firstRow = str_pad($firstRow, 11, '0', STR_PAD_RIGHT);

        // modify diodes on 3,6,9th cause the should be red
        $redDiodeIndex = array(3,6,9);
        foreach($redDiodeIndex as $index){
            if($firstRow[--$index] == 'Y')
                $firstRow[$index] = 'R';
        }

        // compose second row
        $oneMultiples = $time - ($fiveMultiples*5);
        $secondRow = str_repeat('Y',$oneMultiples);
        $secondRow = str_pad($secondRow, 4, '0', STR_PAD_RIGHT);

        return sprintf("%s|%s", $firstRow, $secondRow);
    }

}