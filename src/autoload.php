<?php
// exceptions
require __DIR__ . '/common.php';

/**
 * autoload
 * @param $className
 */
function autoload_cata_5($className){
    $className = explode('\\',$className);
    $className = array_pop($className);

    require __DIR__ . '/'.$className . '.php';
}

spl_autoload_register('autoload_cata_5');