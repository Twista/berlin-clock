<?php

namespace Twista\codeKata;

/**
 * Class SecondsLight
 * @package Twista\codeKata
 */
class SecondsLight implements IObserver {

    /**
     * index of time fragments
     */
    const INDEX = 2;

    /**
     * @param IObservable $observable
     */
    public function update(IObservable $observable) {
        if(!$observable instanceof IDiodeClock)
            return;

        $time = $this->resolveTime($observable->getTimeFragment(self::INDEX));
        $observable->setLights(self::INDEX, $time);
    }

    /**
     * @param $time
     * @return int|string
     */
    protected function resolveTime($time){
        if(($time % 2) == 0)
            return 'Y';
        return 0;
    }
}