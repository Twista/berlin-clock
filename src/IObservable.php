<?php

namespace Twista\codeKata;


/**
 * Interface IObservable
 * @package Twista\codeKata
 */
interface IObservable {
    public function attach(IObserver $observer);
    public function detach(IObserver $observer);
    public function notify();
}